#                               Speed Typer

### Présentation du contexte : 

Durant le cursus de la formation il nous était  demandé de développer un jeux en Javasript sans framework. 

On avait 4 semaine pour réaliser ce jeux .

### Conception : 

- Réalisation de diagramme de Use Case : 

  /home/maroua/js-game/img/UseCase.png

- Réalisation de diagramme de Classe : 

  /home/maroua/rockpaperscissors/images/diagClass.png

- Réalisation de la maquette fonctionnelle .

  /home/maroua/js-game/img/mockup1.png



### Organisation 

Afin d'organiser mon travail et d'avoir un plan à suivre j'ai utiliser des fonctionnalités de Gitlab : 

des sprints , des listes , et des taches .

