# Speed Typer 


## Présentation du contexte : 

Durant le cursus de la formation il nous était  demandé de développer un jeu en Javasript sans framework. 

On avait 4-5 semaine pour réaliser ce jeux.

## Conception :

* Réalisation de diagramme de Use Case : 

En réalisant un diagramme de Use Case j'ai pu mettre en valeur les fonctionnalités de mon jeu .

 <img src="img/UseCase.png" width="800" height="500" />

* Réalisation de la maquette fonctionnelle :

Le maquettage de mon projet était facile à faire car j'avais qu'une seule page html à réaliser et dans cette page y'avait pas  trop d'éléments à manipuler .


 <img src="img/mockup1.png" width="800" height="500" />


## Oraganisation : 
Afin d'organiser mon travail et d'avoir un plan à suivre j'ai utiliser des fonctionnalités de Gitlab : 

des sprints , des listes , et des taches .

Voici un exemple de mon deuxième et dernier sprint :

  <img src="img/exempleSprint.png" width="800" height="500" />



###### 

## Démo : 

dans mon jeux j'avais commencer par la création d'une  liste des mots .

Chaque Level de mon jeu contient une liste bien déterminée .

C'est pour cela j'ai décidé de faire un tableau des tableaux . 

<img src="img/arraydemo.png" width="800" height="500" />

Puis , j'ai voulu faire des effets qui apparaissent chaque fois que je rechargerais ma page .

il y'aura un cercle qui part de droite à gauche puis des gauche à droite en changant de couleur

<img src="img/onloaddemo.png" width="800" height="500" /><img src="img/cssEffect.png" width="800" height="500" />

Ensuite , je voulais que les couleurs changent à chaque fois que j'avançais dans le jeu : 

<img src="img/rotationdemo.png" width="800" height="500" />



When the play fails to type a word before the counter equals 0 then everything gets to its initial state 

<img src="img/gameoverdemo.png" width="800" height="500" />

Si le timer n'est pas à zero le player peut encore taper des mots .

<img src="img/levelchange.png" width="800" height="500" />

Score + High score 

<img src="img/scorehighscore.png" width="800" height="500" />







### How to play the game 

this game is a typing speed game 

What you'll be doing is typing words before the timer is over : Timer is at 4 seconds for the first level.

When you complete each level the Timer will have 1 second less than the previous : timer : 3 seconds 

Some nice effects ( made with css )are shown after typing each word .

There's a list of the words you'll be typing (I though it would make it easier for some people but i doubt it . Anyways..)

I'm proud of myself cause I followed the plan I made before even beginning to code. That showcases that I actually can and should follow a plan . cause it's more efficient when you have a plan 

and know exactly what to do and what to follow .